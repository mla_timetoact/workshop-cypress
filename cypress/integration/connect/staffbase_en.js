Cypress.on('uncaught:exception', (err, runnable) => {
  // returning false here prevents Cypress from
  // failing the test
  return false
})

describe('Deutsche Post Grundlagen', () => {
  beforeEach(() => {
    cy.visit('/', {
      onBeforeLoad(win) {
        Object.defineProperty(win.navigator, 'language', { value: 'en-gb' });
        Object.defineProperty(win.navigator, 'languages', { value: ['en'] });
        Object.defineProperty(win.navigator, 'accept_languages', { value: ['en'] });
      },
      headers: {
        'Accept-Language': 'en',
        "Accept-Encoding": "br, gzip, deflate"
      }
    })
    cy.login(Cypress.env('staffbase_user'), Cypress.env('staffbase_password'), 'en');
  })
  
  it('login success', () => {
    cy.get('.user-menu-btn').should('be.visible');
  })

  after(() => {
    cy.clearCookies()
    cy.get('.user-menu-btn').click();
    cy.contains('Abmelden').click();
  })
})
