Cypress.on('uncaught:exception', (err, runnable) => {
  // returning false here prevents Cypress from
  // failing the test
  return false
})

describe('Deutsche Post Grundlagen', () => {
  beforeEach(() => {
    cy.viewport(1300, 900);
    cy.visit('/'+ Cypress.env('test_site'), {
      headers: {
        "Accept-Encoding": "br, gzip, deflate"
      }
    })
    Cypress.Cookies.preserveOnce('OptanonAlertBoxClosed', 'OptanonConsent')
  })
  
  it('cookie banner close', () => {    
    cy.get('#onetrust-pc-sdk').should('be.visible');
    cy.get('#accept-recommended-btn-handler').should('be.visible');
    cy.get('#accept-recommended-btn-handler').click();
    cy.get('#onetrust-pc-sdk').should('not.be.visible');
  })

  it('headline check', () => {
    cy.contains('Aktuelle Informationen').should('be.visible');
    cy.contains('Aktuelle Informationen').should('have.css', 'color', 'rgb(255, 255, 255)');
  })
  
  it('amount stamps', () => {
    cy.get('.column-1.parsys').find('.teaser-btn.grey').should('have.length', 6);
  })
  
  it('search', () => {
    cy.get('.header-nav-container .search-flyout').should('not.be.visible');
    cy.get('.header-nav-container .open-search').should('be.visible');
    cy.get('.header-nav-container .open-search').click();
    cy.get('.header-nav-container .search-flyout').should('be.visible');
    cy.get('.header-nav-container .search-flyout').find('input[type="text"]').type("post");
    cy.get('.header-nav-container #rta_id_searchButton').click();
    cy.get('.result-item').should(($div) => {
      expect($div).to.have.length.of.at.least(4)
    })
  })

  it('quickaccess', () => {
    cy.get('.quickaccess').should('be.visible');
    cy.get('.quickaccess li').last().should('have.css', 'border-bottom-left-radius', '4px','border-bottom-right-radius', '4px');
    cy.get('.quickaccess li').eq(6).should('not.have.css', 'border-bottom-left-radius', '4px','border-bottom-right-radius', '4px');
  })

  after(() => {
    cy.clearCookies()
  })
})
