import 'cypress-iframe';

Cypress.on('uncaught:exception', (err, runnable) => {
  // returning false here prevents Cypress from
  // failing the test
  return false
})

describe('Deutsche Post Grundlagen', () => {  
  before(() => {
    cy.visit('https://dhltest.staffbase.com', {
      onBeforeLoad(win) {
        Object.defineProperty(win.navigator, 'language', { value: 'de-de' });
        Object.defineProperty(win.navigator, 'languages', { value: ['de'] });
        Object.defineProperty(win.navigator, 'accept_languages', { value: ['de'] });
      },
      headers: {
        'Accept-Language': 'de',
        "Accept-Encoding": "br, gzip, deflate"
      }
    })
    cy.login(Cypress.env('staffbase_user'), Cypress.env('staffbase_password'), 'de');
  })
  
  it('form test', () => {    
    cy.get('.user-menu-btn').should('be.visible');
    cy.wait(10000);
    cy.visit('https://dhltest.staffbase.com/content/form/617937498ca76575bfb39f3f');
    cy.get('iframe');
    cy.iframe('iframe').find('.sb-field__label ').eq(0).should('have.text', 'Willkommen zum Cypress.io Training 27.10.2021');
    cy.iframe('iframe').find('.sb-textfield').eq(0).type('Vorname');
    cy.iframe('iframe').find('.sb-textfield').eq(1).type('Nachname');
    cy.iframe('iframe').find('button[type="submit"]').click();
    cy.iframe('iframe').find('h1').should('have.text', 'Cypress Form successfully submitted');
  })

  after(() => {
    cy.clearCookies()
    cy.get('.user-menu-btn').click();
    cy.contains('Abmelden').click();
  })
})
