// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
import 'cypress-file-upload';
import 'cypress-audit/commands';

Cypress.Commands.add('login', (user, password, langague) => { 
  if(langague === 'de') {
    cy.get('.public-login-hint button').click();
    cy.get('.signin-container').contains('ohne Firmen-E-Mail-Adresse').click();
    cy.get('.signin-form input[placeholder="Ihr Nutzername"]').should('be.visible');
    cy.get('.signin-form input[placeholder="Ihr Nutzername"]').type(user);
    cy.get('.signin-form input[placeholder="Ihr Passwort"]').should('be.visible');
    cy.get('.signin-form input[placeholder="Ihr Passwort"]').type(password);
    cy.get('.signin-form .type-submit').click();
  }
  if(langague === 'en') {
    cy.get('.public-login-hint button').click();
    cy.get('.signin-container').contains('without company e-mail address').click();
    cy.get('.signin-form input[placeholder="Your username"]').should('be.visible');
    cy.get('.signin-form input[placeholder="Your username"]').type(user);
    cy.get('.signin-form input[placeholder="Your password"]').should('be.visible');
    cy.get('.signin-form input[placeholder="Your password"]').type(password);
    cy.get('.signin-form .type-submit').click();
  }
 })

//
//
// -- This is a child command --
// Cypress.Commands.add('drag', { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add('dismiss', { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite('visit', (originalFn, url, options) => { ... })
