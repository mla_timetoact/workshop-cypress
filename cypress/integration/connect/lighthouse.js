import 'cypress-iframe';

Cypress.on('uncaught:exception', (err, runnable) => {
  // returning false here prevents Cypress from
  // failing the test
  return false
})

describe('Deutsche Post Grundlagen', () => {  
  before(() => {
    cy.visit('https://dhltest.staffbase.com', {
      onBeforeLoad(win) {
        Object.defineProperty(win.navigator, 'language', { value: 'de-de' });
        Object.defineProperty(win.navigator, 'languages', { value: ['de'] });
        Object.defineProperty(win.navigator, 'accept_languages', { value: ['de'] });
      },
      headers: {
        'Accept-Language': 'de',
        "Accept-Encoding": "br, gzip, deflate"
      }
    })
    
  })

  it('login', () => {    
    cy.login(Cypress.env('staffbase_user'), Cypress.env('staffbase_password'), 'de');
    cy.get('.user-menu-btn').should('be.visible');
    cy.wait(10000);
  })

  it('lighthouse', () => {    
    cy.visit('https://dhltest.staffbase.com/content/dpdhlitservices.dpdhlappcorner/60f7acae65402e12586f461f');    
    cy.lighthouse({
      performance: 85,
      accessibility: 100,
      "best-practices": 85,
      pwa: 100,
    });
  })
})
