Cypress.on('uncaught:exception', (err, runnable) => {
  // returning false here prevents Cypress from
  // failing the test
  return false
})

describe('Deutsche Post Grundlagen', () => {
  beforeEach(() => {
    cy.visit('/', {
      headers: {
        "Accept-Encoding": "br, gzip, deflate"
      }
    })
    Cypress.Cookies.preserveOnce('OptanonAlertBoxClosed', 'OptanonConsent')
  })
  
  it('cookie banner close', () => {    
    cy.get('#onetrust-pc-sdk').should('be.visible');
    cy.get('#accept-recommended-btn-handler').should('be.visible');
    cy.get('#accept-recommended-btn-handler').click();
    cy.get('#onetrust-pc-sdk').should('not.be.visible');
  })

  it('headline check', () => {
    cy.contains('Aktuelle Informationen').should('be.visible');
    cy.contains('Aktuelle Informationen').should('have.css', 'color', 'rgb(255, 255, 255)');
  })
  
  it('amount stamps', () => {
    cy.get('.column-1.parsys').find('.teaser-btn.grey').should('have.length', 6);
  })
  
  it('search', () => {
    cy.get('.header-mobile-nav-container .search-flyout').should('not.be.visible');
    cy.get('.open-search-mobile').should('be.visible').click();
    cy.get('.header-mobile-nav-container .search-flyout').should('be.visible');
    cy.get('.header-mobile-nav-container .search-flyout').find('input[type="text"]').type("post");
    cy.get('.header-mobile-nav-container #rta_id_searchButton').click();
    cy.get('.result-item').should(($div) => {
      expect($div).to.have.length.of.at.least(4)
    })
  })

  after(() => {
    cy.clearCookies()
  })
})
