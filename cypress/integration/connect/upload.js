Cypress.on('uncaught:exception', (err, runnable) => {
  // returning false here prevents Cypress from
  // failing the test
  return false
})

describe('File Upload', () => {  
  before(() => {
    cy.visit('https://imgur.com/upload');
  })
  
  it('upload test', () => {    
    cy.get('body').find('#file-input').attachFile('test.jpg');
    cy.pause();
  })
})
